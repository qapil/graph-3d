#include "constant.h"
#include <windows.h>
#include <commctrl.h>
#include <math.h>
#include <conio.h>
#include <stdio.h>
#define PI 3.14159265

#define SIZE1_W	400
#define SIZE1_H	300
#define SIZE2_W	640
#define SIZE3_W	800
#define SIZE2_H	480
#define SIZE3_H	600

#define X_START -8
#define Y_START -8
#define X_SIZE 32
#define Y_SIZE 32
#define MAX_VALUE 10

struct POINT3D {
	double x, y, z;
};

HWND hWin;

UCHAR whichfn;
UINT size_w, size_h;
BOOL malir, bufok, rotation = FALSE;
HDC GrafDC, WinDC;
HBITMAP hBitmap, oldBitmap;
UINT timer;
int rx, ry;

double sin_t[361];
double cos_t[361];
HLOCAL hfn, hrot, hdisp, hfillbuf;
double *fn;
POINT3D *rot, mpt[4];
POINT *disp;
char *fillbuf;
int x_ofs, y_ofs, z_ofs;

/**************************************/

double function0(double x, double y)
{
	double p;

	if ( x == 0 && y == 0 ) p = MAX_VALUE;
	else p = 4*cos( sqrt(2*(x*x + y*y)) ) / sqrt(x*x + y*y);

	return p;
}
double function1(double x, double y)
{
	return -(x*x+y*y)/20 +5;
}
double function2(double x, double y)
{
	return x/3*sin(x-y);
}

/**************************************/

void sincos_table()
{
	for (int i=0; i <= 360; i ++) {
		sin_t[i] = sin(i*PI/180);
		cos_t[i] = cos(i*PI/180);
	}
}
void eval_func(double *p, double (*func)(double, double))
{
	int i, j;

	for (i = 0; i < X_SIZE; i ++)
		for (j = 0; j < Y_SIZE; j ++)
			*(p +i+j*Y_SIZE) = func(i*0.5+X_START+0.25, j*0.5+Y_START+0.25);
}
void reset(POINT3D *p3, double *p)
{
	int i, j;
    
	for (i=0; i < X_SIZE; i ++)
    	for (j=0; j < Y_SIZE; j ++) {
			(p3 +i+j*Y_SIZE)->x = i*0.5+X_START+0.25;
			(p3 +i+j*Y_SIZE)->y = j*0.5+Y_START+0.25;
			(p3 +i+j*Y_SIZE)->z = *(p +i+j*Y_SIZE);
        }
}

void rot_alfa(POINT3D *pts, int angle)
{
	POINT3D p;
	int i, j;

    while (angle < 0) angle += 360;
    while (angle >= 360) angle -= 360;

    for (i = 0; i < 4; i ++) {
		p = mpt[i];
		mpt[i].y = p.y*cos_t[angle] - p.z*sin_t[angle];
		mpt[i].z = p.y*sin_t[angle] + p.z*cos_t[angle];
    }

	for (i = 0; i < X_SIZE; i ++)
		for (j = 0; j < Y_SIZE; j ++) {
			p = *(pts +i+j*Y_SIZE);

			(pts +i+j*Y_SIZE)->y = p.y*cos_t[angle] - p.z*sin_t[angle];
			(pts +i+j*Y_SIZE)->z = p.y*sin_t[angle] + p.z*cos_t[angle];
		}
}
void rot_beta(POINT3D *pts, int angle)
{
	POINT3D p;
	int i, j;

    while (angle < 0) angle += 360;
    while (angle >= 360) angle -= 360;

    for (i = 0; i < 4; i ++) {
		p = mpt[i];
		mpt[i].z = p.z*cos_t[angle] - p.x*sin_t[angle];
		mpt[i].x = p.z*sin_t[angle] + p.x*cos_t[angle];
    }

	for (i = 0; i < X_SIZE; i ++)
		for (j = 0; j < Y_SIZE; j ++) {
			p = *(pts +i+j*Y_SIZE);

			(pts +i+j*Y_SIZE)->z = p.z*cos_t[angle] - p.x*sin_t[angle];
			(pts +i+j*Y_SIZE)->x = p.z*sin_t[angle] + p.x*cos_t[angle];
		}
}
void rot_gama(POINT3D *pts, int angle)
{
	POINT3D p;
	int i, j;

    while (angle < 0) angle += 360;
    while (angle >= 360) angle -= 360;

    for (i = 0; i < 4; i ++) {
		p = mpt[i];
		mpt[i].x = p.x*cos_t[angle] - p.y*sin_t[angle];
		mpt[i].y = p.x*sin_t[angle] + p.y*cos_t[angle];
    }

	for (i = 0; i < X_SIZE; i ++)
		for (j = 0; j < Y_SIZE; j ++) {
			p = *(pts +i+j*Y_SIZE);

			(pts +i+j*Y_SIZE)->x = p.x*cos_t[angle] - p.y*sin_t[angle];
			(pts +i+j*Y_SIZE)->y = p.x*sin_t[angle] + p.y*cos_t[angle];
		}
}

void InitFunc(double *fn, POINT3D *rot)
{
    switch (whichfn) {
    	case 0: eval_func(fn, function0); break;
        case 1: eval_func(fn, function1); break;
	case 2: eval_func(fn, function2); break;
    }
    reset(rot, fn);

	mpt[0].x =  1.0; mpt[0].y =  1.0; mpt[0].z = 0.0;
	mpt[1].x = -1.0; mpt[1].y =  1.0; mpt[1].z = 0.0;
    mpt[2].x = -1.0; mpt[2].y = -1.0; mpt[2].z = 0.0;
    mpt[3].x =  1.0; mpt[3].y = -1.0; mpt[3].z = 0.0;

    rot_alfa(rot, 120);
    rot_beta(rot, 30);
    rot_gama(rot, 20);
}
int InitGraf()
{
	if ((hfn = LocalAlloc(LHND, X_SIZE*Y_SIZE*sizeof(double))) == NULL) return -1;
	if ((fn = (double *) LocalLock(hfn)) == NULL) {
    	LocalFree(hfn);
        return -1;
    }

    if ((hrot = LocalAlloc(LHND, X_SIZE*Y_SIZE*sizeof(POINT3D))) == NULL) {
    	LocalUnlock(hfn); LocalFree(hfn);
    	return -1;
    }
    if ((rot = (POINT3D *) LocalLock(hrot)) == NULL) {
    	LocalFree(hrot);
    	LocalUnlock(hfn); LocalFree(hfn);
    	return -1;
    }

    if ((hdisp = LocalAlloc(LHND, X_SIZE*Y_SIZE*sizeof(POINT))) == NULL) {
    	LocalUnlock(hrot); LocalFree(hrot);
    	LocalUnlock(hfn); LocalFree(hfn);
    	return -1;
    }
    if ((disp = (POINT *) LocalLock(hdisp)) == NULL) {
    	LocalFree(hdisp);
    	LocalUnlock(hrot); LocalFree(hrot);
    	LocalUnlock(hfn); LocalFree(hfn);
    	return -1;
    }

	sincos_table();
    InitFunc(fn, rot);

    return 0;
}
void DestroyGraf()
{
   	LocalUnlock(hdisp); LocalFree(hdisp);
   	LocalUnlock(hrot); LocalFree(hrot);
    LocalUnlock(hfn); LocalFree(hfn);
}

void add_perspective(POINT *to, POINT3D *from)
{
	int i, j;
	double p;

	for (i = 0; i < X_SIZE; i ++)
		for (j = 0; j < Y_SIZE; j ++) {
			p = (from +i+j*Y_SIZE)->z + z_ofs;

			(to +i+j*Y_SIZE)->x = (long)(1024*(from +i+j*Y_SIZE)->x / p) + x_ofs;
			(to +i+j*Y_SIZE)->y = (long)(1024*(from +i+j*Y_SIZE)->y / p) + y_ofs;
		}
}

inline void putpixel(char *fillbuf, int x, int y)
{
	if (x < 0) x = 0;
	else if (x >= (int) size_w) x = size_w-1;
	if (y < 0) y = 0;
	else if (y >= (int) size_h) y = size_w-1;

    if (*(fillbuf +x+y*size_w) == 0) 
	*(fillbuf +x+y*size_w) = 1;
    else
	*(fillbuf +x+y*size_w) = 0;
}
void fillline(char *fillbuf, POINT a1, POINT a2)
{
    int dx, dy, px, k1, k2,
	x, y;
    POINT p;
    char a;
    
    dx = a2.x - a1.x;
    dy = a2.y - a1.y;
    
    if (dy < 0) {
		dx = -dx; dy = -dy;
		p = a1; a1 = a2; a2 = p;
    }
    if (dx < 0) a = -1;
    else a = 1;
    
    px = -dy;
    k1 = 2*a*dx;
    k2 = -2*dy;

    x = a1.x; y = a1.y;
    putpixel(fillbuf, x, y);
    while (y++ < a2.y) {
		px += k1;
		while (px >= 0) {
			x += a;
			px += k2;
		}
		putpixel(fillbuf, x, y);
    }
}
void fillarea(HDC DC, char *fillbuf, POINT b1, POINT b2, COLORREF color)
{
    int i, j, first, x;
    
    for (j = b1.y; j <= b2.y; j ++) {
		first = 1;
		for (i = b1.x; i <= b2.x; i ++)
			if (*(fillbuf +i+j*size_w) == 1) {
				*(fillbuf +i+j*size_w) = 0;
				if (first) {
					first = 0;
					x = i;
				} else {
					first = 1;
					MoveToEx(DC, x, j, NULL);
					LineTo(DC, i, j);
				}
			}
    }
}
void tetrafill(HDC DC, char *fillbuf, POINT a1, POINT a2, POINT a3, COLORREF color)
{
    POINT a5, b1, b2;
    int d1, d2, d3;
    
//	InvalidateRect(hWin, NULL, TRUE);
//	MessageBox(hWin, "", "", MB_OK);

	HPEN pen = CreatePen(PS_SOLID, 1, color);
	DeleteObject(SelectObject(DC, pen));
    
    d1 = a2.y - a1.y;
    d2 = a3.y - a2.y;
    d3 = a1.y - a3.y;
    
    if ((d1 > 0 && d2 > 0) || (d1 < 0 && d2 < 0)) {
		if (d1 > 0) a5.y = a2.y - 1;
		else a5.y = a2.y + 1;
		a5.x = a2.x - ((a2.x - a1.x) / abs(d1));
		fillline(fillbuf, a1, a5);
    } else 
		if (d1 != 0) fillline(fillbuf, a1, a2);

    if ((d2 > 0 && d3 > 0) || (d2 < 0 && d3 < 0)) {
		if (d2 > 0) a5.y = a3.y - 1;
		else a5.y = a3.y + 1;
		a5.x = a3.x - ((a3.x - a2.x) / abs(d2));
		fillline(fillbuf, a2, a5);
    } else 
		if (d2 != 0) fillline(fillbuf, a2, a3);

    if ((d3 > 0 && d1 > 0) || (d3 < 0 && d1 < 0)) {
		if (d3 > 0) a5.y = a1.y - 1;
		else a5.y = a1.y + 1;
		a5.x = a1.x - ((a1.x - a3.x) / abs(d3));
		fillline(fillbuf, a3, a5);
    } else 
		if (d3 != 0) fillline(fillbuf, a3, a1);

    b1 = b2 = a1;
    if (a2.x < b1.x) b1.x = a2.x;
    else if (a2.x > b2.x) b2.x = a2.x;
    if (a3.x < b1.x) b1.x = a3.x;
    else if (a3.x > b2.x) b2.x = a3.x;

    if (a2.y < b1.y) b1.y = a2.y;
    else if (a2.y > b2.y) b2.y = a2.y;
    if (a3.y < b1.y) b1.y = a3.y;
    else if (a3.y > b2.y) b2.y = a3.y;

    fillarea(DC, fillbuf, b1, b2, color);

    MoveToEx(DC, a1.x, a1.y, NULL);
    LineTo(DC, a2.x, a2.y);
    LineTo(DC, a3.x, a3.y);
    LineTo(DC, a1.x, a1.y);
}
void quadrafill(HDC DC, char *fillbuf, POINT a1, POINT a2, POINT a3, POINT a4, COLORREF color)
{
    POINT a5, b1, b2;
    int d1, d2, d3, d4;
    
//	InvalidateRect(hWin, NULL, TRUE);
//	MessageBox(hWin, "", "", MB_OK);

	HPEN pen = CreatePen(PS_SOLID, 1, color);
	DeleteObject(SelectObject(DC, pen));
    
    d1 = a2.y - a1.y;
    d2 = a3.y - a2.y;
    d3 = a4.y - a3.y;
    d4 = a1.y - a4.y;
    
    if ((d1 > 0 && d2 > 0) || (d1 < 0 && d2 < 0)) {
		if (d1 > 0) a5.y = a2.y - 1;
		else a5.y = a2.y + 1;
		a5.x = a2.x - ((a2.x - a1.x) / abs(d1));
		fillline(fillbuf, a1, a5);
    } else 
		if (d1 != 0) fillline(fillbuf, a1, a2);

    if ((d2 > 0 && d3 > 0) || (d2 < 0 && d3 < 0)) {
		if (d2 > 0) a5.y = a3.y - 1;
		else a5.y = a3.y + 1;
		a5.x = a3.x - ((a3.x - a2.x) / abs(d2));
		fillline(fillbuf, a2, a5);
    } else 
		if (d2 != 0) fillline(fillbuf, a2, a3);

    if ((d3 > 0 && d4 > 0) || (d3 < 0 && d4 < 0)) {
		if (d3 > 0) a5.y = a4.y - 1;
		else a5.y = a4.y + 1;
		a5.x = a4.x - ((a4.x - a3.x) / abs(d3));
		fillline(fillbuf, a3, a5);
    } else 
		if (d3 != 0) fillline(fillbuf, a3, a4);

    if ((d4 > 0 && d1 > 0) || (d4 < 0 && d1 < 0)) {
		if (d4 > 0) a5.y = a1.y - 1;
		else a5.y = a1.y + 1;
		a5.x = a1.x - ((a1.x - a4.x) / abs(d4));
		fillline(fillbuf, a4, a5);
    } else 
		if (d4 != 0) fillline(fillbuf, a4, a1);

    b1 = b2 = a1;
    if (a2.x < b1.x) b1.x = a2.x;
    else if (a2.x > b2.x) b2.x = a2.x;
    if (a3.x < b1.x) b1.x = a3.x;
    else if (a3.x > b2.x) b2.x = a3.x;
    if (a4.x < b1.x) b1.x = a4.x;
    else if (a4.x > b2.x) b2.x = a4.x;

    if (a2.y < b1.y) b1.y = a2.y;
    else if (a2.y > b2.y) b2.y = a2.y;
    if (a3.y < b1.y) b1.y = a3.y;
    else if (a3.y > b2.y) b2.y = a3.y;
    if (a4.y < b1.y) b1.y = a4.y;
    else if (a4.y > b2.y) b2.y = a4.y;

    fillarea(DC, fillbuf, b1, b2, color);

    MoveToEx(DC, a1.x, a1.y, NULL);
    LineTo(DC, a2.x, a2.y);
    LineTo(DC, a3.x, a3.y);
    LineTo(DC, a4.x, a4.y);
    LineTo(DC, a1.x, a1.y);
}
inline void drawrect(HDC DC, char *fillbuf, POINT *pts, POINT3D *rot, int i, int j)
{
	int a1, a2, a3, a4;
	COLORREF col1, col2;
	POINT3D n, p1, p2;

	a1 = i  +j    *Y_SIZE;
	a2 = i+1+j    *Y_SIZE;
	a3 = i+1+(j+1)*Y_SIZE;
	a4 = i  +(j+1)*Y_SIZE;

//---------//

	p1.x = (rot+a2)->x - (rot+a1)->x; p2.x = (rot+a3)->x - (rot+a1)->x;
	p1.y = (rot+a2)->y - (rot+a1)->y; p2.y = (rot+a3)->y - (rot+a1)->y;
	p1.z = (rot+a2)->z - (rot+a1)->z; p2.z = (rot+a3)->z - (rot+a1)->z;
	n.x = p1.x*p2.y - p2.x*p1.y;
	n.y = p1.y*p2.z - p2.y*p1.z;
	n.z = p1.z*p2.x - p2.z*p1.x;

	col1 = 192*(0.5 + ((double)(n.y-n.z-n.x) / (2*sqrt(3)*sqrt(n.x*n.x + n.y*n.y + n.z*n.z))));

	p1.x = (rot+a3)->x - (rot+a1)->x; p2.x = (rot+a4)->x - (rot+a1)->x;
	p1.y = (rot+a3)->y - (rot+a1)->y; p2.y = (rot+a4)->y - (rot+a1)->y;
	p1.z = (rot+a3)->z - (rot+a1)->z; p2.z = (rot+a4)->z - (rot+a1)->z;
	n.x = p1.x*p2.y - p2.x*p1.y;
	n.y = p1.y*p2.z - p2.y*p1.z;
	n.z = p1.z*p2.x - p2.z*p1.x;

	col2 = 192*(0.5 + ((double)(n.y-n.z-n.x) / (2*sqrt(3)*sqrt(n.x*n.x + n.y*n.y + n.z*n.z))));

//---------//
	col1 = RGB((UCHAR) col1, (UCHAR) col1, (UCHAR) col1);
	col2 = RGB((UCHAR) col2, (UCHAR) col2, (UCHAR) col2);
	tetrafill(DC, fillbuf,	*(pts+a1), *(pts+a2), *(pts+a3), col1);
	tetrafill(DC, fillbuf,	*(pts+a1), *(pts+a3), *(pts+a4), col2);
}				
void mdraw(HDC DC, POINT *pts)
{
	memset(fillbuf, 0, size_w*size_h);
	
	int i, j, p;

	HPEN pen = CreatePen(PS_SOLID, 1, 0x00000000);
	HPEN oldpen = SelectObject(DC, pen);
	
	if ((mpt[0].z <= mpt[1].z) && (mpt[0].z <= mpt[2].z) && (mpt[0].z <= mpt[3].z))
	    if (mpt[3].z <= mpt[1].z) p = 5;
	    else p = 6;
	else if ((mpt[1].z <= mpt[0].z) && (mpt[1].z <= mpt[2].z) && (mpt[1].z <= mpt[3].z))
	    if (mpt[0].z <= mpt[2].z) p = 7;
	    else p = 8;
	else if ((mpt[2].z <= mpt[0].z) && (mpt[2].z <= mpt[1].z) && (mpt[2].z <= mpt[3].z))
	    if (mpt[1].z <= mpt[3].z) p = 1;
	    else p = 2;
	else 
	    if (mpt[2].z <= mpt[0].z) p = 3;
	    else p = 4;

//	char s[20];
//	sprintf(s, "%i", p);
//	TextOut(DC, 10, 0, s, 1);

	switch (p) {
	case 1:
		for (i = X_SIZE-2; i >= 0; i --)
		    for (j = Y_SIZE-2; j >= 0; j --)
				drawrect(DC, fillbuf, pts, rot, i, j);
		break;
	case 2:
		for (j = Y_SIZE-2; j >= 0; j --)
		    for (i = X_SIZE-2; i >= 0; i --)
				drawrect(DC, fillbuf, pts, rot, i, j);
		break;
	case 3:
		for (j = Y_SIZE-2; j >= 0; j --)
		    for (i = 0; i < X_SIZE-1; i ++)
				drawrect(DC, fillbuf, pts, rot, i, j);
		break;
	case 4:
		for (i = 0; i < X_SIZE-1; i ++)
		    for (j = Y_SIZE-2; j >= 0; j --)
				drawrect(DC, fillbuf, pts, rot, i, j);
		break;
	case 5:
		for (i = 0; i < X_SIZE-1; i ++)
		    for (j = 0; j < Y_SIZE-1; j ++)
				drawrect(DC, fillbuf, pts, rot, i, j);
		break;
	case 6:
		for (j = 0; j < Y_SIZE-1; j ++)
		    for (i = 0; i < X_SIZE-1; i ++)
				drawrect(DC, fillbuf, pts, rot, i, j);
		break;
	case 7:
		for (j = 0; j < Y_SIZE-1; j ++)
		    for (i = X_SIZE-2; i >= 0; i --)
				drawrect(DC, fillbuf, pts, rot, i, j);
		break;
	case 8:
		for (i = X_SIZE-2; i >= 0; i --)
		    for (j = 0; j < Y_SIZE-1; j ++)
				drawrect(DC, fillbuf, pts, rot, i, j);
		break;
	}

	DeleteObject(SelectObject(GrafDC, oldpen));
}
void draw(POINT *pts)
{
	if (malir && bufok) mdraw(GrafDC, pts);
	else {
		int i, j;
		HPEN oldpen;

		oldpen = SelectObject(GrafDC, GetStockObject(WHITE_PEN));
		for (i = 0; i < X_SIZE-1; i ++) {
			for (j = 0; j < Y_SIZE-1; j ++) {
				MoveToEx(GrafDC, (pts +i+j*Y_SIZE)->x, (pts +i+j*Y_SIZE)->y, NULL);
				LineTo(GrafDC, (pts +i+(j+1)*Y_SIZE)->x, (pts +i+(j+1)*Y_SIZE)->y);
			}

			for (j = 0; j < Y_SIZE; j ++) {
				MoveToEx(GrafDC, (pts +i+j*Y_SIZE)->x, (pts +i+j*Y_SIZE)->y, NULL);
				LineTo(GrafDC, (pts +i+1+j*Y_SIZE)->x, (pts +i+1+j*Y_SIZE)->y);
			}
		}
		MoveToEx(GrafDC, (pts +i)->x, (pts +i)->y, NULL);
		for (j = 0; j < Y_SIZE-1; j ++) {
	 		LineTo(GrafDC, (pts +i+(j+1)*Y_SIZE)->x, (pts +i+(j+1)*Y_SIZE)->y);
		}
		SelectObject(GrafDC, oldpen);
	}
}

void KresliGraf()
{
	add_perspective(disp, rot);
    PatBlt(GrafDC, 0, 0, size_w, size_h, BLACKNESS);
    draw(disp);
}

/**************************************/

void CheckFunc(HMENU menu, int item)
{
	static UINT last = item;

    CheckMenuItem(menu, last, MF_BYCOMMAND + MF_UNCHECKED);
    CheckMenuItem(menu, item, MF_BYCOMMAND + MF_CHECKED);
    last = item;
}
void CheckSize(HMENU menu, int item)
{
	static UINT last = item;

    CheckMenuItem(menu, last, MF_BYCOMMAND + MF_UNCHECKED);
    CheckMenuItem(menu, item, MF_BYCOMMAND + MF_CHECKED);
    last = item;
}

void InitBitmap()
{
 	HDC DC = GetDC(0);
    GrafDC = CreateCompatibleDC(DC);

	hBitmap = CreateCompatibleBitmap(DC, size_w, size_h);
    ReleaseDC(0, DC);
    oldBitmap = SelectObject(GrafDC, hBitmap);
    PatBlt(GrafDC, 0,0, size_w, size_h, BLACKNESS);

    bufok = 0;
    if ((hfillbuf = LocalAlloc(LHND, size_w*size_h*sizeof(char))) == NULL) return;
    if ((fillbuf = (char *) LocalLock(hfillbuf)) == NULL) {
    	LocalFree(hfillbuf);
		return;
    }
    bufok = 1;
}
void DoneBitmap()
{
   	LocalUnlock(hfillbuf); LocalFree(hfillbuf);
	SelectObject(GrafDC, oldBitmap);
	DeleteObject(hBitmap);
	DeleteDC(GrafDC);
}

void Paint(HWND hWnd)
{
	PAINTSTRUCT ps;

	BeginPaint(hWnd, &ps);

    BitBlt(ps.hdc, ps.rcPaint.left, ps.rcPaint.top,
 
	ps.rcPaint.right-ps.rcPaint.left, ps.rcPaint.bottom-ps.rcPaint.top,
    	GrafDC, ps.rcPaint.left, ps.rcPaint.top, SRCCOPY);

	EndPaint(hWnd, &ps);
}

void rotate()
{
    if (rx) rot_beta(rot, -rx);
    if (ry) rot_alfa(rot, ry);
    if (rx || ry) {
        KresliGraf();
		BitBlt(WinDC, 0, 0, size_w, size_h,	GrafDC, 0, 0, SRCCOPY);
    }
}
	
LRESULT CALLBACK MainWndProc(HWND hWnd, UINT msg, WPARAM wP, LPARAM lP)
{
    RECT r1, r2;
    static int x1, y1, x2, y2;
    static BOOL pressed;

	switch (msg) {
    case WM_COMMAND:
    	switch (LOWORD(wP)) {
        case CM_FUNCTION1:
        	if (whichfn == 0) break;
            whichfn = 0;
            CheckFunc(GetMenu(hWnd), CM_FUNCTION1);
            InitFunc(fn, rot);
            KresliGraf();
            InvalidateRect(hWnd, NULL, FALSE);
            break;
        case CM_FUNCTION2:
        	if (whichfn == 1) break;
            whichfn = 1;
            CheckFunc(GetMenu(hWnd), CM_FUNCTION2);
            InitFunc(fn, rot);
            KresliGraf();
            InvalidateRect(hWnd, NULL, FALSE);
            break;
        case CM_FUNCTION3:
        	if (whichfn == 2) break;
            whichfn = 2;
            CheckFunc(GetMenu(hWnd), CM_FUNCTION3);
            InitFunc(fn, rot);
            KresliGraf();
            InvalidateRect(hWnd, NULL, FALSE);
            break;

        case CM_SIZE1:
        	if (size_w == SIZE1_W && size_h == SIZE1_H) break;
			DestroyGraf();
        	DoneBitmap();
        	size_w = SIZE1_W; size_h = SIZE1_H;
            x_ofs = size_w /2;
            y_ofs = size_h /2;
            z_ofs = 75;
            CheckSize(GetMenu(hWnd), CM_SIZE1);
            InitBitmap();
			InitGraf();
            KresliGraf();
            GetClientRect(hWnd, &r1);
            GetWindowRect(hWnd, &r2);
            r2.right -= r2.left;
            r2.bottom -= r2.top;
        	SetWindowPos(hWnd, 0, 0, 0, size_w+r2.right-r1.right, size_h+r2.bottom-r1.bottom,
            	SWP_NOMOVE | SWP_NOZORDER);
            InvalidateRect(hWnd, NULL, TRUE);
			ReleaseDC(hWin, WinDC);
			WinDC = GetDC(hWin);
            break;
        case CM_SIZE2:
        	if (size_w == SIZE2_W && size_h == SIZE2_H) break;
        	DestroyGraf();
			DoneBitmap();
        	size_w = SIZE2_W; size_h = SIZE2_H;
            x_ofs = size_w /2;
            y_ofs = size_h /2;
            z_ofs = 50;
            CheckSize(GetMenu(hWnd), CM_SIZE2);
            InitBitmap();
			InitGraf();
            KresliGraf();
            GetClientRect(hWnd, &r1);
            GetWindowRect(hWnd, &r2);
            r2.right -= r2.left;
            r2.bottom -= r2.top;
        	SetWindowPos(hWnd, 0, 0, 0, size_w+r2.right-r1.right, size_h+r2.bottom-r1.bottom,
            	SWP_NOMOVE | SWP_NOZORDER);
            InvalidateRect(hWnd, NULL, TRUE);
			ReleaseDC(hWin, WinDC);
			WinDC = GetDC(hWin);
            break;
        case CM_SIZE3:
        	if (size_w == SIZE3_W && size_h == SIZE3_H) break;
        	DestroyGraf();
			DoneBitmap();
        	size_w = SIZE3_W; size_h = SIZE3_H;
            x_ofs = size_w /2;
            y_ofs = size_h /2;
            z_ofs = 40;
            CheckSize(GetMenu(hWnd), CM_SIZE3);
            InitBitmap();
			InitGraf();
            KresliGraf();
            GetClientRect(hWnd, &r1);
            GetWindowRect(hWnd, &r2);
            r2.right -= r2.left;
            r2.bottom -= r2.top;
        	SetWindowPos(hWnd, 0, 0, 0, size_w+r2.right-r1.right, size_h+r2.bottom-r1.bottom,
            	SWP_NOMOVE | SWP_NOZORDER);
            InvalidateRect(hWnd, NULL, FALSE);
			ReleaseDC(hWin, WinDC);
			WinDC = GetDC(hWin);
            break;

        case CM_MALIR:
        	malir = !malir;
            if (malir)
			    CheckMenuItem(GetMenu(hWnd), CM_MALIR, MF_BYCOMMAND | MF_CHECKED);
            else
			    CheckMenuItem(GetMenu(hWnd), CM_MALIR, MF_BYCOMMAND | MF_UNCHECKED);
			PatBlt(GrafDC, 0,0, size_w, size_h, BLACKNESS);
			draw(disp);
            InvalidateRect(hWnd, NULL, FALSE);
        	break;

        case CM_HELP:
        	MessageBox(hWnd, "Nen� je�t� implementov�no\na asi ani nebude.", "Help", MB_OK);
            break;

        case CM_QUIT:
			PostQuitMessage(0);
        }
        break;
	case WM_TIMER:
		if (rx) rot_beta(rot, -rx);
		if (ry) rot_alfa(rot, ry);
		if (rx || ry) {
			KresliGraf();
			BitBlt(WinDC, 0, 0, size_w, size_h,	GrafDC, 0, 0, SRCCOPY);
		}
		break;
    case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		if (rotation) {
			KillTimer(hWnd, timer);
			rotation = FALSE;
		}
    	SetCapture(hWnd);
    	pressed = TRUE;
        x1 = LOWORD(lP); y1 = HIWORD(lP);
    	break;
    case WM_LBUTTONUP:
    	ReleaseCapture();
    	pressed = FALSE;
    	break;
	case WM_RBUTTONUP:
    	ReleaseCapture();
    	pressed = FALSE;
		rx = x2; ry = y2;
		if ((timer = SetTimer(hWnd, 1000, 40, NULL)) != 0)
			rotation = TRUE;
		break;
    case WM_MOUSEMOVE:
    	if (pressed) {
			x2 = LOWORD(lP)-x1;
            if (x2) {
            	x1 = LOWORD(lP);
            	rot_beta(rot, -x2);
            }
            y2 = HIWORD(lP)-y1;
            if (y2) {
            	y1 = HIWORD(lP);
            	rot_alfa(rot, y2);
            }
            if (x2 || y2) {
             	KresliGraf();
				BitBlt(WinDC, 0, 0, size_w, size_h,	GrafDC, 0, 0, SRCCOPY);
            }
        }
    	break;
    case WM_CREATE:
    	InitBitmap();
        if (InitGraf() == 0) KresliGraf();
        else {
        	MessageBox(hWnd, "Chyba p�i alokaci pam�ti. Aplikace skon��.", "Error", MB_OK);
            DoneBitmap();
            PostQuitMessage(0);
        }
        break;
    case WM_PAINT:
    	Paint(hWnd);
        break;
    case WM_CLOSE:
    	DestroyGraf();
		DoneBitmap();
        PostQuitMessage(0);
    	break;
	default:
	    return DefWindowProc(hWnd, msg, wP, lP);
	}

   return 0;
}

BOOL InitApplication(HINSTANCE hInstance)
{
    WNDCLASSEX wc;

    wc.cbSize		 = sizeof(WNDCLASSEX);
    wc.style         = 0;
    wc.lpfnWndProc   = MainWndProc;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = hInstance;
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH) (COLOR_WINDOW+1);
    wc.lpszMenuName  = "MAINMENU";
    wc.lpszClassName = "GrafWndClass";
    wc.hIconSm		 = LoadIcon(NULL, IDI_QUESTION);

    return RegisterClassEx(&wc);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	whichfn = 0;
   	size_w = SIZE1_W; size_h = SIZE1_H;
    x_ofs = size_w /2;
    y_ofs = size_h /2;
    z_ofs = 75;

    hWin = CreateWindow(
    	"GrafWndClass", "Graf funkce dvou prom�nn�ch",
		WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX,
	    CW_USEDEFAULT, CW_USEDEFAULT, size_w, size_h,
        NULL, NULL, hInstance, NULL
    );
    if (!hWin) return FALSE;


	CheckFunc(GetMenu(hWin), CM_FUNCTION1);
    CheckSize(GetMenu(hWin), CM_SIZE1);

	malir = FALSE;
    ShowWindow(hWin, nCmdShow);
    UpdateWindow(hWin);
	WinDC = GetDC(hWin);

    return TRUE;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR, int nCmdShow )
{
    MSG msg;

    if (!hPrevInstance)
        if (!InitApplication(hInstance)) return FALSE;
    if (!InitInstance(hInstance, nCmdShow)) return FALSE;

    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
    	DispatchMessage(&msg);
    }
	ReleaseDC(hWin, WinDC);

 	return msg.wParam;
}
